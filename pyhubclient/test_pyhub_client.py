from unittest import TestCase

import requests_cache

from . import PyhubClient


class install_cache:

    """ Context manager for working with cache. """

    REQUEST_CACHE_PATH = 'pyhubclient/test_cache/'

    def __init__(self, title):
        self.title = title

    def __enter__(self):
        requests_cache.core.install_cache(
            cache_name=self.REQUEST_CACHE_PATH + self.title,
            allowable_methods=('GET', 'POST'),
            allowable_codes=(200, 401, 403, 404),
        )

    def __exit__(self, t, value, traceback):
        requests_cache.uninstall_cache()


class PyhubClientTest(TestCase):

    def setUp(self):
        self.pyhub_client = PyhubClient()

    def test_unbind_bindable_status_not_found(self):
        with install_cache(self.id()):
            response = self.pyhub_client.unbind_bindable_status(
                PyhubClient.HUB, '000000000000'
            )
            self.assertEqual(response.status_code, 404)

    def test_unbind_bindable_status_already_bound(self):
        with install_cache(self.id()):
            response = self.pyhub_client.unbind_bindable_status(
                PyhubClient.HUB, '9DEEE2C7087D'
            )
            self.assertEqual(response.status_code, 200)
            self.assertEqual(
                response.json(),
                {
                    'status': 401,
                    'output': 'Device is already bound.Use bind_token to get the status'  # noqa
                }
            )

    def test_unbind_bindable_status_bad(self):
        with install_cache(self.id()):
            response = self.pyhub_client.unbind_bindable_status(
                PyhubClient.HUB, '000C4300000B'
            )
            self.assertEqual(response.status_code, 200)
            self.assertEqual(
                response.json(),
                {
                    'status': 200,
                    'output': {
                        'status': 'bad',
                        'label': '000C4300000B',
                        'server': ''
                    }
                },
            )

    def test_unbind_bindable_status_good(self):
        with install_cache(self.id()):
            response = self.pyhub_client.unbind_bindable_status(
                PyhubClient.HUB, '000C43000021'
            )
            self.assertEqual(response.status_code, 200)
            self.assertEqual(
                response.json(),
                {
                    'status': 200,
                    'output': {
                        'status': 'good',
                        'label': '000C43000021',
                        'server': ''
                    }
                },
            )

    def test_bind_not_exist(self):
        with install_cache(self.id()):
            response = self.pyhub_client.bind(PyhubClient.HUB, '000000000000')
            self.assertEqual(response.status_code, 404)

    def test_bind_already_bound(self):
        with install_cache(self.id()):
            response = self.pyhub_client.bind(PyhubClient.HUB, '9DEEE2C7087D')
            self.assertEqual(response.status_code, 401)

    def test_bind_offline(self):
        with install_cache(self.id()):
            response = self.pyhub_client.bind(PyhubClient.HUB, '000C4300000B')
            self.assertEqual(response.status_code, 200)
            self.assertEqual(
                response.json(),
                {'status': 408, 'output': '000C4300000B offline'},
            )
