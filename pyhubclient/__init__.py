import base64
import datetime
import hashlib
import hmac
import json
import logging
import requests

logger = logging.getLogger(__name__)


def sign_sha256(message, secret):
    signer = hmac.new(bytes(secret, 'utf-8'), digestmod=hashlib.sha256)
    signer.update(bytes(message, 'utf-8'))
    return base64.urlsafe_b64encode(signer.digest()).decode('utf-8')


class PyhubException(Exception):
    pass


class PyhubClient:

    PYHUB_SERVER_URL = 'https://54.173.253.78'

    SOCKET = 'DEVICE_RFM_SOCKET'
    LIGHTBULB = 'DEVICE_ZIGBEE'
    HUB = 'DEVICE_HUB'
    METER = 'DEVICE_METER'

    CONTROL_ON = 'CONTROL_ON'
    CONTROL_OFF = 'CONTROL_OFF'
    CONTROL_READ = 'CONTROL_READ'
    CONTROL_SCAN = 'CONTROL_SCAN'
    CONTROL_WRITE = 'CONTROL_WRITE'

    def __init__(self, bind_token=None, bind_secret=None):
        self.bindable = self.bindable_signed = None
        if bind_token and bind_secret:
            self.bindable = self._get_bindable(bind_token)
            self.bindable_signed = self._get_bindable_signed(
                bind_token, bind_secret
            )
        logger.debug(
            'Bindable: {}, '
            'bindable_signed: {}.'.format(self.bindable, self.bindable_signed)
        )

    def _get_bindable(self, bind_token):
        request = {
            'utc': int(datetime.datetime.utcnow().timestamp()),
            'bind_token': bind_token,
        }
        return base64.urlsafe_b64encode(
            bytes(json.dumps(request), 'utf-8')
        ).decode('utf-8')

    def _get_bindable_signed(self, bind_token, bind_secret):
        bindable_encoded = self._get_bindable(bind_token)
        return sign_sha256(bindable_encoded, bind_secret)

    def request(self, url, method_name, params):
        global json_response
        try:
            logger.info(
                'Pyhub request: {}, {}, {}.'.format(url, method_name, params))
            response = getattr(requests, method_name)(
                url, params=params, verify=False
            )
        except requests.ConnectionError as err:
            msg = 'Pyhub connection error: {}'.format(err)
            logger.error(msg)
            raise PyhubException(msg)
        except requests.RequestException as err:
            msg = 'Pyhub request error: {}'.format(err)
            logger.error(msg)
            raise PyhubException(msg)
        else:
            # if response.status_code in (401, 403):
                # raise PyhubException(response.content)

            logger.info('{}, {}.'.format(response, response.content))
            return response

    def unbind_bindable_status(self, device, device_label):
        return self.request(
            self.PYHUB_SERVER_URL + '/user/bindable_status.json',
            'get',
            params={
                'device': device,
                'device_label': device_label,
            }
        )

    def bindable_status(self):
        return self.request(
            self.PYHUB_SERVER_URL + '/user/bindable_status.json',
            'get',
            params={
                'bindable': self.bindable,
                'bindable_signed': self.bindable_signed,
            }
        )

    def bind(self, device, device_label):
        return self.request(
            self.PYHUB_SERVER_URL + '/user/bind.json',
            'post',
            params={
                'device': device,
                'device_label': device_label,
                'bindable': self.bindable,
                'bindable_signed': self.bindable_signed,
            }
        )

    def unbind(self):
        return self.request(
            self.PYHUB_SERVER_URL + '/user/bind.json',
            'delete',
            params={
                'bindable': self.bindable,
                'bindable_signed': self.bindable_signed,
            }
        )

    def pair(self, device_type, id):
        return self.request(
            self.PYHUB_SERVER_URL + '/user/pair.json',
            'post',
            params={
                'device': device_type,
                'device_label': id,
                'bindable': self.bindable,
                'bindable_signed': self.bindable_signed,
            },
        )

    def unpair(self, device_type, id):
        return self.request(
            self.PYHUB_SERVER_URL + '/user/pair.json',
            'delete',
            params={
                'device': device_type,
                'device_label': id,
                'bindable': self.bindable,
                'bindable_signed': self.bindable_signed,
            },
        )

    def control(self, device_type, id, action):
        return self.request(
            self.PYHUB_SERVER_URL + '/user/control.json',
            'post',
            params={
                'device': device_type,
                'device_label': id,
                'device_control': action,
                'bindable': self.bindable,
                'bindable_signed': self.bindable_signed,
            },
        )

    def changelevel(self, device_type, id, level, transition_time=0):
        return self.request(
            self.PYHUB_SERVER_URL + '/user/control.json',
            'post',
            params={
                'device': device_type,
                'device_label': id,
                'device_control': self.CONTROL_WRITE,
                'control_data': json.dumps({
                    'level_control': {
                        'level': level,
                        'transition_time': transition_time,
                    }
                }),
                'bindable': self.bindable,
                'bindable_signed': self.bindable_signed,
            },
        )
