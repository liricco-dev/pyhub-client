from distutils.core import setup

setup(
    name='pyhub-client',
    version='0.0.2',
    packages=['pyhubclient'],
    url='http://192.168.1.111:3000/jay.pun/pyhub-client',
    license='',
    author='Pun Ka Fai',
    author_email='jp@liricco.com',
    description='Pyhub client'
)
