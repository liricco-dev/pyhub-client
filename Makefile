# System variables
ENV=$(CURDIR)/.env
PYTHON=$(ENV)/bin/python
REQUIREMENTS=requirements-test.txt

all: $(ENV)

# target: pypi_upload - Upload package to pypi.python.org
pypi_upload:
	@$(PYTHON) setup.py sdist upload

# target: help - Display callable targets
help:
	@grep -e "^# target:" [Mm]akefile | sed -e 's/^# target: //g'

# target: test - Run tests
test: $(ENV)
	@$(PYTHON) -m unittest discover
	

# target: test_ci - Run tests command adapt for CI systems
test_ci:
	@python -m unittest discover


$(ENV): $(REQUIREMENTS)
	[ -d $(ENV) ] || virtualenv --no-site-packages -p /usr/bin/python3.4 $(ENV)
	$(ENV)/bin/pip install -M -r $(REQUIREMENTS)
	touch $(ENV)
